import sys

# returns P(C|a1,a2, ... , an)
def naive_bayes(attr_probabilities_c1, attr_probabilities_c2, class_prob_c1, class_prob_c2):
    e_yes = []

    pi_c1 = 1
    pi_c2 = 1

    for i in range(0, len(attr_probabilities_c2)):
        pi_c1 *= attr_probabilities_c1[i]
        pi_c2 *= attr_probabilities_c2[i]

    pi_c1 *= class_prob_c1
    pi_c2 *= class_prob_c2


    e_yes.append(pi_c1 / (pi_c1+pi_c2))
    e_yes.append(pi_c2 / (pi_c1+pi_c2))

    return e_yes


def classify_instance(attr_t_probabilities_c1, class_prob_c1, attr_t_probabilities_c2, class_prob_c2, attributes):
    instance_probabilities_c1 = []
    instance_probabilities_c2 = []

    i = 0
    for attr in attributes:
        if attr == '1':
            instance_probabilities_c1.append(attr_t_probabilities_c1[i])
            instance_probabilities_c2.append(attr_t_probabilities_c2[i])
        else:
            instance_probabilities_c1.append(1 - attr_t_probabilities_c1[i])
            instance_probabilities_c2.append(1 - attr_t_probabilities_c2[i])
        i += 1

    return naive_bayes(instance_probabilities_c1, instance_probabilities_c2, class_prob_c1, class_prob_c2)


def print_probabilities(c1_attr, c2_attr):
    inv_c1 = []
    inv_c2 = []

    for i in range(0, len(c1_attr)):
        inv_c1.append(1 - c1_attr[i])
        inv_c2.append(1 - c2_attr[i])

    print()
    print("Attribute Probabilities P(x1, ... , xn | Spam)")
    
    for x in range(len(c1_attr)):
        print("P(x" + str(x) + " = 1 | Spam) = " + str(c1_attr[x]))
        print("P(x" + str(x) + " = 0 | Spam) = " + str(inv_c1[x]))
    
    print()
    print("Attribute Probabilities P(x1, ... , xn | Ham)")
    
    for x in range(len(c1_attr)):
        print("P(x" + str(x) + " = 1 | Spam) = " + str(c2_attr[x]))
        print("P(x" + str(x) + " = 0 | Spam) = " + str(inv_c2[x]))
    
    print()


# returns an array of probability for the attributes being true given c
# P(a1,a2, ... , an|c)
def assign_probabilities(class_set):
    store = []
    attribute_size = len(class_set[0])

    for i in range(0, attribute_size):
        store.append(0)

    for instance_i in class_set:
        count = 0
        for attribute in instance_i:
            if attribute == '1':
                store[count] += 1
            count += 1

    total = len(class_set)
    for i in range(0, len(store)):
        store[i] /= total

    return store


if __name__ == '__main__':
    if len(sys.argv) == 3:
        
        with open(sys.argv[1]) as data:
            data = data.readlines()

        spam = []
        notSpam = []

        for line in data:
            x = line.split()

            if x[len(x) - 1] == '1':
                spam.append(x[:len(x) - 1])
            else:
                notSpam.append(x[:len(x) - 1])

        # probabilities of Ck
        p_spam = len(spam)/len(data)
        p_not_spam = len(notSpam)/len(data)

        # probabilities of X1, ... , Xn that are true
        spam_attr_prob = assign_probabilities(spam)
        not_spam_attr_prob = assign_probabilities(notSpam)

        print_probabilities(spam_attr_prob, not_spam_attr_prob)

        with open(sys.argv[2]) as test_file:
            test_file = test_file.readlines()

        test_data = []

        for line in test_file:
            x = line.split()
            test_data.append(x)

        print("Unlabelled classified: P(Spam|x0, ... ,xn) compared with P(Ham|x0, ... ,xn)")
        for instance in test_data:
            classify = classify_instance(spam_attr_prob, p_spam, not_spam_attr_prob, p_not_spam, instance)
            if classify[0] > classify[1]:
                print("Class -> Spam:  P(Spam|X) = " + "{0:.3f}".format(classify[0]*100)
                    + "%, P (Ham|X)  = " + "{0:.3f}%".format(classify[1]*100))
            elif classify[0] < classify[1]:
                print("Class -> Ham :  P(Ham|X)  = " + "{0:.3f}".format((classify[1]*100))
                    + "%, P (Spam|X) = " + "{0:.3f}%".format(classify[0]*100))
            else:
                print("Undefined, equal probabilities: p = " + str((classify[1]*100)) + "%")

        # print(str(count/len(test_data)))
    else:
        print("Invalid Arguments: training, data")
        
